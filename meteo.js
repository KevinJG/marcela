// api key : d811adcfaa8766bb187ff395e8862744

// Sélection des élements
const iconElement = document.querySelector(".meteo-icon");
const tempElement = document.querySelector(".meteo-temperature p");

// Déclaration de notre objet meteo
const meteo = {};

meteo.temperature = {
    unit: "celsius"
}

// Déclaration de la valeur KELVIN
const KELVIN = 273;
// Notre API key
const key = "d811adcfaa8766bb187ff395e8862744";

// Appel de la météo via l'API OpenWeather
function getMeteo(ville,pays) {
    let api = `https://api.openweathermap.org/data/2.5/weather?q=${ville},${pays}&appid=${key}`;

    fetch(api)
        .then(function(response) {
            let data = response.json();
            return data;
        })
        .then(function(data){
            meteo.temperature.value = Math.floor(data.main.temp - KELVIN);
            meteo.vent = data.wind.speed;
            meteo.iconId = data.weather[0].icon;
            console.log(data);
        })
        .then(function() {
            showMeteo();
        })
};

// On montre tout ça à l'UI
function showMeteo() {
    iconElement.innerHTML = `<img src="icons/${meteo.iconId}.png" >`;
    tempElement.innerHTML = `${meteo.temperature.value}°<span>Celsius - Vitesse du vent : </span>${meteo.vent} m/s`;
}

getMeteo("Vannes","FR");
