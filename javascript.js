/******************************************************************************
SLIDESHOW
******************************************************************************/

let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) { 
  showSlides(slideIndex += n);
}

function minusSlides(n) {
  showSlides(slideIndex -= n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {

  let i;
  let slides = document.getElementsByClassName("mySlides");
  let thumbnails = document.getElementsByClassName("thumbnail");
  let captionText = document.getElementById("caption");

  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}

  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }

  for (i = 0; i < thumbnails.length; i++) {
    thumbnails[i].className = thumbnails[i].className.replace(" active", "");
  }

  slides[slideIndex-1].style.display = "block";
  thumbnails[slideIndex-1].className += " active";
  captionText.innerHTML = thumbnails[slideIndex-1].alt;
}
    
function autoSlide() {
  slideIndex++;
  showSlides(slideIndex);
}

let launch = setInterval(autoSlide,2500);

for (j = 0; j < 8; j++) {
    document.getElementsByClassName("clear")[j].addEventListener("click", function() {
        clearInterval(launch);
    })
}